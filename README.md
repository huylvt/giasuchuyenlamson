DATABASE SCHEMA
===============

Table auth_user
---------------

| Column                       |Type            |Null |Key  |Comment|
|------------------------------|----------------|-----|-----|-------|
| id                           | int(11)        | NO  | PRI |       |
| username                     | varchar(30)    | NO  | UNI |       |
| first_name                   | varchar(30)    | NO  |     |       |
| last_name                    | varchar(30)    | NO  |     |       |
| email                        | varchar(75)    | NO  | UNI |       |
| password                     | varchar(128)   | NO  |     |       |
| is_staff                     | tinyint(1)     | NO  |     |       |
| is_active                    | tinyint(1)     | NO  |     |       |
| is_superuser                 | tinyint(1)     | NO  |     |       |
| last_login                   | datetime       | NO  |     |       |
| date_joined                  | datetime       | NO  |     |       |
| status                       | varchar(2)     | NO  |     |       |
| email_key                    | varchar(32)    | YES |     |       |
| avatar_typ                   | varchar(1)     | NO  |     |       |
| country                      | varchar(2)     | NO  |     |       |
| show_country                 | tinyint(1)     | NO  |     |       |
| date_of_birth                | date           | YES |     |       |
| interesting_tags             | longtext       | NO  |     |       |
| ignored_tags                 | longtext       | NO  |     |       |
| email_tag_filter_strategy    | smallint(6)    | NO  |     |       |
| display_tag_filter_strategy  | smallint(6)    | NO  |     |       |
| consecutive_days_visit_count | int(11)        | NO  |     |       |

username: The unique username for a user in the LamSonAcademy system. It can contain alphanumerics and the special 
characters shown within the brackets: [ _ @ + - . ]. The username is the only user-provided information that other users 
can currently see. EdX has never allowed users to change usernames, but might do so in the future.

first_name: Not used; a user’s full name is stored in auth_userprofile.name instead.

last_name: Not used; a user’s full name is stored in auth_userprofile.name instead.

email: The user’s email address, which is the primary mechanism users use to log in.
This value is optional by default in Django, but is required by LamSonAcademy. This value must be unique to each user and 
is never shown to other users.

password: A hashed version of the user’s password. Depending on when the password was last set, this will either be a 
SHA1 hash or PBKDF2 with SHA256 (Django 1.3 uses the former and 1.4 the latter).

is_staff: Most users have a 0 for this field. Set to 1 if the user is a staff member of LamSonAcademy, with corresponding
elevated privileges that cut across courses. It does not indicate that the person is a member of the course team for any given course.
Generally, users with this flag set to 1 are either edX partner managers responsible for course delivery, or edX developers who need access for testing and debugging purposes.
Users who have is_staff = 1 have Admin privileges on all courses and can see additional debug information on the Instructor Dashboard.

is_active: This value is 1 if the user has clicked on the activation link that was sent to them when they created their account, and 0 otherwise.
Users who have is_active = 0 generally cannot log into the system. However, when users first create an account, they are automatically logged in even though they have not yet activated the account. This is to let them experience the site immediately without having to check their email. A message displays on the dashboard to remind users to check their email and activate their accounts when they have time. When they log out, they cannot log back in again until activation is complete. However, because edX sessions last a long time, it is possible for someone to use the site as a student for days without being “active”.
Once is_active is set to 1, it is only set back to 0 if the user is banned (which is a very rare, manual operation).

is_superuser: Controls access to django_admin views. Set to 1 (true) only for site admins. 0 for almost everybody.
History: Only the earliest developers of the system have this set to 1, and it is no longer really used in the codebase.

last_login: A datetime of the user’s last login. Should not be used as a proxy for activity, since people can use the site all the time and go days between logging in and out.

date_joined: Date that the account was created.


Table auth_userprofile
----------------------


|Column	                    | Type	        |Null | Key	|Comment                                 |
|---------------------------|---------------|-----|-----|----------------------------------------|
| id	                    | int(11)       | NO  | PRI |                                        |
| user_id                   | int(11)       | NO  | UNI	|                                        |
| name                      | varchar(255)  | NO  | MUL |                                        |
| language                  | varchar(255)  | NO  | MUL	| # Obsolete                             |
| location                  | varchar(255)  | NO  | MUL | # Obsolete                             |
| meta                      | longtext      | NO  |     |                                        |
| courseware                | varchar(255)  | NO  |     | # Obsolete                             |
| gender                    | varchar(6)    | YES | MUL	| # Only users signed up after prototype |
| mailing_address           | longtext      | YES |     | # Only users signed up after prototype |
| year_of_birth             | int(11)	    | YES | MUL	| # Only users signed up after prototype |
| level_of_education        | varchar(6)    | YES | MUL | # Only users signed up after prototype |
| goals	                    | longtext      | YES |     | # Only users signed up after prototype |
| allow_certificate         | tinyint(1)    | NO  |     |                                        |
| country                   | varchar(2)    | YES |     |                                        |
| city                      | longtext      | YES |     |                                        |
| bio	                    | varchar(3000) | YES |     |                                        |
| profile_image_uploaded_at | datetime      | YES |     |                                        |

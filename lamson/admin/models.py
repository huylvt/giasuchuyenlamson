# -*- coding: utf-8 -*-
'''
Created on Jul 27, 2015

@author: huylvt
'''
from datetime import datetime
from lamson.extensions import db


courses = db.Table('courses',
                   db.Column('course_id',
                             db.Integer,
                             db.ForeignKey('course.id')),
                   db.Column('user_id',
                             db.Integer,
                             db.ForeignKey('user.id')))


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), unique=True)
    name = db.Column(db.String(100), unique=True)
    email = db.Column(db.String(30))
    social_id = db.Column(db.String(64), nullable=False, unique=True)
    password = db.Column(db.String(128))
    is_staff = db.Column(db.Integer)
    is_active = db.Column(db.Integer)
    is_superuser = db.Column(db.Integer)
    last_login = db.Column(db.DateTime, default=datetime.now)
    date_joined = db.Column(db.DateTime, default=datetime.now)
    courses = db.relationship('Course', secondary=courses,
                              backref=db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return '<User %r>' % (self.name)

    def avatar(self):
        if 'twitter' in self.social_id:
            uri = 'profile_image?size=original'
            return 'https://twitter.com/{}/{}'.format(self.username, uri)


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    slug = db.Column(db.String(64))
    icon = db.Column(db.String(20))
    parent_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    parent = db.relationship("Category", backref='children', remote_side=[id])
    courses = db.relationship('Course', backref='category', lazy='dynamic')

    def __repr__(self):
        return '<Category %r>' % (self.name)


class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    slug = db.Column(db.String(64))
    description = db.Column(db.String(4096))
    image = db.Column(db.String(4096))
    start = db.Column(db.DateTime, default=datetime.now)
    end = db.Column(db.DateTime, default=datetime.now)
    price = db.Column(db.Integer, default=0)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    lectures = db.relationship('Lecture', backref='course', lazy='dynamic')
    announcements = db.relationship('Announcement', backref='course', lazy='dynamic')

    def __repr__(self):
        return '<Course %r>' % self.name


class Lecture(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    video_uri = db.Column(db.String(1028))
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    
    def __repr__(self):
        return '<Lecture %r>' % self.title


class Announcement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    


# -*- coding: utf-8 -*-
'''
Created on Jul 27, 2015

@author: huylvt
'''
from flask import (Blueprint, render_template, redirect,
                   url_for, flash, g, session)
from lamson.admin.models import User, Category, Course
from lamson.admin.oauth import OAuthSignIn
from lamson.admin.decorators import login_required
from lamson.api.views import load_user
from lamson.extensions import db


admin = Blueprint('admin', __name__, url_prefix='/lsadmin')
admin.before_request(load_user)


@admin.route('/authorize/<provider>')
def oauth_authorize(provider):
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@admin.route('/callback/<provider>')
def oauth_callback(provider):
    oauth = OAuthSignIn.get_provider(provider)
    social_id, username, name, email = oauth.callback()
    if social_id is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))
    user = User.query.filter_by(social_id=social_id).first()
    if not user:
        user = User(social_id=social_id,
                    username=username,
                    name=name,
                    email=email)
        db.session.add(user)
        db.session.commit()
    session['user_id'] = str(user.username)
    g.user = user
    return redirect(url_for('admin.home'))


@admin.route("/login")
def login():
    return render_template("admin/login.html")


@admin.route('/')
@login_required
def home():
    categories = Category.query.all()
    courses = Course.query.all()
    return render_template('admin/home.html', categories=categories, courses=courses)

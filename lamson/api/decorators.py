# -*- coding: utf-8 -*-
from functools import wraps
from flask import g, redirect, url_for, abort


def superuser_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return redirect(url_for('admin.login'))
        if not g.user.is_superuser:
            abort(403)
        return f(*args, **kwargs)
    return decorated_function
        
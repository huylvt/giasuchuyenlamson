# -*- coding: utf-8 -*-
from flask import Blueprint, session, g
from lamson.admin.models import User, Category, Course
from lamson.api.decorators import superuser_required


api = Blueprint('api', __name__, url_prefix='/api')


def load_user():
    if "user_id" in session.keys() and session["user_id"]:
        user = User.query.filter_by(username=session["user_id"]).first()
    else:
        user = None
    g.user = user


@api.route("/create/course", methods=["POST"])
@superuser_required
def create_course():
    return True
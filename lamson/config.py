# -*- coding: utf-8 -*-
'''
Created on Jul 11, 2015

@author: huylvt
'''
import os


class BaseConfig(object):

    PROJECT = "lamson"

    # Get app root path, also can use flask.root_path.
    # ../../config.py
    PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

    DEBUG = True
    TESTING = True

    ADMINS = ['huylvt.vn@gmail.com']

    # http://flask.pocoo.org/docs/quickstart/#sessions
    SECRET_KEY = 'b594481f-f3b2-49d0-9fc6-29c1d0859d2e'

    # Fild upload, should override in production.
    # Limited the maximum allowed payload to 16 megabytes.
    # http://flask.pocoo.org/docs/patterns/fileuploads/#improving-uploads


class DefaultConfig(BaseConfig):

    BASEDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # Flask-Sqlalchemy: http://packages.python.org/Flask-SQLAlchemy/config.html
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # SQLITE for prototyping.
    if BaseConfig.DEBUG:
        SQLALCHEMY_DATABASE_URI = "sqlite:///" + BASEDIR + "/lamson.db"
    # MYSQL for production.
    else:
        SQLALCHEMY_DATABASE_URI = "mysql://lamson:lams0n**@127.0.0.1/lamson"

    # Rauth: https://rauth.readthedocs.org/en/latest/
    OAUTH_CREDENTIALS = {
        'facebook': {
            'id': '1014337935280690',
            'secret': 'd587f19f4ef4f60bd1c6f8a73d3d9943'
        },
        'twitter': {
            'id': 'OmoGFhsDOtb4BZgzsEuKkPBsi',
            'secret': 'CQcLdXGDB219G2mabX95XWyHeus3EZ3YZR7ft9YSzEnzKTiH8y'
        }
    }
    # Flask-babel: http://pythonhosted.org/Flask-Babel/
    ACCEPT_LANGUAGES = ['vi']
    BABEL_DEFAULT_LOCALE = 'en'

    # Flask-cache: http://pythonhosted.org/Flask-Cache/
    CACHE_TYPE = 'simple'
    CACHE_DEFAULT_TIMEOUT = 60

    # Flask Uploads
    UPLOAD_FOLDER = BASEDIR + '/uploads'
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

    # Flask-mail: http://pythonhosted.org/flask-mail/
    # https://bitbucket.org/danjac/flask-mail/issue/3/problem-with-gmails-smtp-server
    MAIL_DEBUG = BaseConfig.DEBUG
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    # Should put MAIL_USERNAME and MAIL_PASSWORD
    # in production under instance folder.
    MAIL_USERNAME = 'yourmail@gmail.com'
    MAIL_PASSWORD = 'yourpass'
    MAIL_DEFAULT_SENDER = MAIL_USERNAME

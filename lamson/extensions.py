# -*- coding: utf-8 -*-
'''
Created on Jul 28, 2015

@author: huylvt
'''
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.migrate import Migrate


db = SQLAlchemy()

migrate = Migrate()

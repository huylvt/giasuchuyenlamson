# -*- coding: utf-8 -*-
'''
Created on Jul 27, 2015

@author: huylvt
'''
from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Optional


class ContactForm(Form):
    name = StringField('name', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])
    mobile = StringField('mobile', validators=[Optional()])
    message = TextAreaField("message")

# -*- coding: utf-8 -*-
'''
Created on Jul 27, 2015

@author: huylvt
'''
from flask import (Blueprint, render_template, request, current_app, send_from_directory)
from lamson.admin.models import Category
from lamson.api.views import load_user
from lamson.admin.decorators import login_required


frontend = Blueprint('frontend', __name__)
frontend.before_request(load_user)


@frontend.route("/uploads/<path:filename>")
def send_file(filename):
    print current_app.config['UPLOAD_FOLDER']
    return send_from_directory(current_app.config['UPLOAD_FOLDER'], filename)


@frontend.route("/")
@login_required
def home():
    categories = Category.query.all()
    return render_template("home.html", categories=categories)


@frontend.route("/about/lamson")
@frontend.route("/about/gslamson")
def about():
    if request.path.endswith('gslamson'):
        return render_template("about.html")
    else:
        return render_template("aboutls.html")


@frontend.route("/price", methods=["GET", "POST"])
def price():
    return render_template("price.html")


@frontend.route("/courses")
def courses():
    return render_template("courses.html")

# -*- coding: utf-8 -*-
'''
Created on Jul 28, 2015

@author: huylvt
'''
from lamson.app import create_app
from lamson.admin.models import Category, Course
from lamson.extensions import db
from flask.ext.script import Manager, Shell
from flask.ext.migrate import MigrateCommand


app = create_app()

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('shell', Shell())


@manager.command
def initdb():
    db.create_all()
    p1 = Category(name='Math', slug='math', icon='calculator')
    db.session.add(p1)
    p2 = Category(name='Basic Math', slug='basic-math')
    p2.parent = p1
    db.session.add(p2)
    c1 = Course(name=u'Hình học lớp 7',
                slug='hinh-hoc-lop-7',
                description=u'Cung cấp kiến thức cơ bản về hình học lớp 7',
                image='hinh-hoc-7.jpg')
    db.session.add(c1)
    p2.courses.append(c1)
    db.session.commit()
    

@manager.command
def run():
    """Run in local machine."""
    app.run()


if __name__ == "__main__":
    manager.run()
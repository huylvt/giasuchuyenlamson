# -*- coding: utf-8 -*-

class Tutor:
  def __init__(self, info):
    self.info = info
  
  @property
  def id(self):
    return self.info.get("_id")
  
  @property
  def tutorid(self):
    return self.info.get("tutorid")
  
  @property
  def name(self):
    return self.info.get("name")
  
  @property
  def mobile(self):
    return self.info.get("mobile")
  
  @property
  def email(self):
    return self.info.get("email")
  
  @property
  def facebook(self):
    return self.info.get("facebook")
  
  @property
  def descriptions(self):
    return self.info.get("descriptions")
  
  @property
  def urlimg(self):
    return self.info.get("urlimg")
  
  @property
  def adults(self):
    return self.info.get("adults")
  
  @property
  def college(self):
    return self.info.get("college")
  
  @property
  def subjects(self):
    return self.info.get("subjects", [])
  
class Classrm:
  def __init__(self, info):
    self.info = info
    
  @property
  def id(self):
    return self.info.get("_id")
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, flash, url_for, redirect, session
from users.models import User
import db, api
admin = Blueprint("admin", __name__,
                  template_folder="templates", static_folder="static")

@admin.route("/lsadmin", methods=["GET", "POST"])
@api.login_required
def lsadmin():
    return redirect("/dashboard")

@admin.route("/dashboard", methods=["GET", "POST"])
@api.login_required
def index():
    title = "Dashboard"
    count_tutors = db.count_all_tutors()
    count_class = db.count_all_class()
    count_class_empty = db.count_class_empty()
    classinfo = db.get_all_class()
    tutors = db.get_all_tutors()
    return render_template("admin/dashboard.html", title=title, count_tutors=count_tutors, tutors=tutors,
                           count_class=count_class, count_class_empty=count_class_empty, classinfo=classinfo)
    
@admin.route("/createclass", methods=["GET", "POST"])
@api.login_required
def createclass():
    message = ""
    if request.method == "POST":
      classid = request.form.get("classid")
      subject = request.form.get("subject")
      level = request.form.get("level")
      parents = request.form.get("parents")
      mobile = request.form.get("mobile")
      address = request.form.get("address")
      fee = request.form.get("fee")
      if not "K" in fee:
        fee += "K"
      pay = request.form.get("pay")
      if not "%" in pay:
        pay += "%"
      require = request.form.get("require")
      status = request.form.get("status")
      time = request.form.get("time")
      tutorid = request.form.get("tutorid")
      note = request.form.get("note")
      count = db.count_all_class()
      _id = 1301 + count
      if subject == "Toán":
        urlimg = "img/class/math.jpg"
        namesubj = "Toán học"
      elif subject == "Văn":
        namesubj = "Văn học"
        urlimg = "img/class/van.jpg"
      elif subject == "Anh":
        namesubj = "Tiếng Anh"
        urlimg = "img/class/english.jpg"
      elif subject == "Lý":
        namesubj = "Vật lý"
        urlimg = "img/class/physical.jpg"
      elif subject == "Hóa":
        namesubj = "Hóa học"
        urlimg = "img/class/chemistry.jpg"
      data = {"_id":int(_id), "classid":classid, "subject":namesubj, "level":level, "parents":parents, "address":address, "note":note,
              "mobile":mobile, "fee":fee, "pay":pay, "require":require, "status":status, "urlimg":urlimg, "tutorid":tutorid, "time":time}
      db.add_new_class(data)
      message = "Add new class successful."
      return render_template("admin/createclass.html", message=message)
    else:
      title = "Create Class"
      return render_template("admin/createclass.html", title=title)

@admin.route("/addtutor", methods=["GET", "POST"])
@api.login_required
def addtutor():
  if request.method == "POST":
    tutorid = request.form.get("tutorid")
    name = request.form.get("name")
    subjects = request.form.getlist("subjects")
    email = request.form.get("email")
    mobile = request.form.get("mobile")
    facebook = request.form.get("facebook")
    college = request.form.get("college")
    descriptions = request.form.get("descriptions")
    adults = request.form.get("adults")
    clc = request.form.get("clc")
    if clc == "Yes":
      quality = True
    else:
      quality = False
    count = db.count_all_tutors()
    _id = 1301 + count
    urlimg = "img/tutor/%s.jpg" %tutorid
    data = {"_id":int(_id), "tutorid":tutorid, "name":name, "subjects":subjects, "email":email, "mobile":mobile, "urlimg":urlimg,
            "facebook":facebook, "college":college, "descriptions":descriptions, "adults":adults, "quality":quality}
    db.add_new_tutors(data)
    message = "Add new tutor successful."
    return render_template("admin/addtutor.html", message=message)
  else:
    title = "Add Tutor"
    return render_template("admin/addtutor.html", title=title)

@admin.route("/editclass", methods=["GET", "POST"])
@api.login_required
def editclass():
  if request.method == "POST":
    classid = request.form.get("classid")
    status = request.form.get("status")
    tutorid = request.form.get("tutorid")
    data = {"status":status, "tutorid":tutorid}    
    db.update_class_info(classid, data)
    return redirect("/dashboard")
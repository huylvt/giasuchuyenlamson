# -*- coding: utf-8 -*-
from functools import wraps
from flask import g, request, redirect, url_for, session
from math import ceil
import smtplib
import settings, db


import sys
reload(sys)
sys.setdefaultencoding("utf-8")


MAIL_SERVER = "smtp.gmail.com:587"
MAIL_USERNAME = "huylvt.vn@gmail.com"
MAIL_PASSWORD = "Xuan30!)"
FROMADDR = "huylvt.vn@gmail.com"
TOADDR = ["giasuchuyenlamson@gmail.com", "tuananhhoang36@facebook.com", "tuananhhoang36@gmail.com"]


def Send_mail(message):
    server = smtplib.SMTP(MAIL_SERVER)
    server.starttls()  
    server.login(MAIL_USERNAME, MAIL_PASSWORD)  
    server.sendmail(FROMADDR, TOADDR, message)  
    server.quit()
  
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        session_id = session.get('session_id')
        if not session_id:
            return redirect('/login')
    
        user_id = db.get_user_id(session_id)
        if not user_id:
            return redirect('/login')
        return f(*args, **kwargs)
    return decorated_function
  
class Pagination(object):

    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in xrange(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num

# -*- coding: utf-8 -*-
from pymongo import Connection
from pymongo import DESCENDING
from lib.pbkdf2 import pbkdf2_bin
from base64 import b64encode, b64decode
from itertools import izip

import uuid
import time
import hashlib
Database = Connection("127.0.0.1:27017").lamsontutor

########## Start Manager User ##########

def sign_in(username, password):
    username = username.strip()
    user = Database.users.find_one({"username": username}, {"password":True, "session_id":True})
    if not user:
        return False
    
    passwd = hashlib.md5(password).hexdigest()
    if passwd == user['password']:
        if user.has_key('session_id'):
            session_id = user['session_id']
        else:
            session_id = hashlib.md5('%s:%s' % (username, time.time())).hexdigest()
            Database.users.update({"_id": user.get('_id')},
                           {'$set': {'session_id': session_id}})
    
        return session_id
    return False

def login(email, passwd):
  email = email.strip()
  user = Database.users.find_one({"email":email}, {"password":True, "session_id":True})
  if not user:
    return False
  
  password = hashlib.md5(passwd).hexdigest()
  if password == user["password"]:
    if user.has_key("session_id"):
      session_id = user["session_id"]
    else:
      session_id = hashlib.md5('%s:%s' %(email, time.ctime())).hexdigest()
      Database.users.update({"_id":user["_id"]}, {'$set':{'session_id':session_id}})
    return session_id
  return False

def logout(session_id):
  Database.users.update({"session_id": session_id},
                       {"$unset": {"session_id": 1}})
  return True

def get_user_id(session_id):
    user = Database.users.find_one({'session_id': session_id}, {'_id': True})
    if user:
        return user.get('_id')

def get_userid_by_username(username):
    user = Database.users.find_one({"username":username})
    if user:
        return user["userid"]
    else:
        return False

def set_sessionid(userid, sessionid):
    sessioninfo = {"userid":userid, "sessionid":sessionid}
    Database.session.save(sessioninfo)
    return True

def remove_sessionid(userid, sessionid):
    sessioninfo = {"userid":userid, "sessionid":sessionid}
    Database.session.remove(sessioninfo)
    return True

def get_user(userid):
    user = Database.users.find_one({"userid":userid})
    return user

def check_user(username, password):
    user = Database.users.find_one({"username":username})
    if user and password == user["password"]:
        return user["_id"]
    else:
        return False

########## End Manage User #########

def get_all_tutors():
  tutor = Database.tutor.find()
  return list(tutor)

def get_all_news():
    news = Database.news.find()
    return list(news)

def get_last_news():
    news = Database.news.find_one()
    return news

def count_all_tutors():
    tutors = Database.tutor.find()
    count = len(list(tutors))
    return count

def get_tutors_for_page(page, per_page):
    hometutor = Database.tutor.find({"quality":True})
    m = list(hometutor)
    tutors = []
    for i in range(1300 + per_page * (page - 1), 1300 + per_page * page):
        for j in m:
            if j["_id"] == i:
                tutors.append(j)
    return tutors

def get_tutor(mobile):
  tutor = Database.tutor.find_one({"mobile":mobile})
  if tutor:
    return tutor
  else:
    return False

def get_new_documents():
    documents = Database.document.find()
    return list(documents)

def get_document_bytitle(title):
    document = Database.document.find_one({"title":title})
    return document

def get_classinfo_for_page(page, per_page):
    classes = Database.classinfo.find()
    m = list(classes)
    classinfo = []
    for i in range(1301 + per_page * (page - 1), 1301 + per_page * page):
        for j in m:
            if j["_id"] == i:
                classinfo.append(j)
    return classinfo

def count_all_class():
    count = Database.classinfo.find().count()
    return count

def count_class_empty():
    classes = []
    c = Database.classinfo.find()
    m = list(c)
    for i in m:
        if i["status"] == "waiting":
            classes.append(i)
    count = len(classes)
    return count 

######################################################################
#                                                                    #
#                   Class Info  (Use in admin backend)               #
#                                                                    #
######################################################################
def get_all_class():
    classinfo = []
    classes = Database.classinfo.find()
    for i in list(classes):
        classinfo.append(i)
    return classinfo
  
def add_new_class(data):
  Database.classinfo.save(data)
  return True

def remove_class(_id):
  _id = int(_id)
  Database.classinfo.remove({"_id":_id})
  return True

def update_class_info(classid, data):
  Database.classinfo.update({"classid":classid}, {"$set":data})
  return True
  
def add_new_tutors(data):
  Database.tutor.save(data)
  return True


#=====================================================================
#             Goc hoc tap theo mo hinh social media
#=====================================================================




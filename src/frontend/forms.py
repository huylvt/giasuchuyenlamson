# -*- coding: utf-8 -*-
from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField, SubmitField, SelectField, validators, ValidationError, PasswordField, BooleanField

class ContactForm(Form):
    name = TextField(u"Họ Tên:")
    email = TextField(u"Email:", [validators.Required(u"Địa chỉ Email không hợp lệ!"), validators.Email(u"Địa chỉ Email không hợp lệ!")])
    mobile = TextField(u"Số ĐT:")
    classroom = SelectField(u'Lớp:', choices=[('lop1', u'Lớp 1'), ('lop2', u'Lớp 2'), ('lop3', u'Lớp 3'), ('lop4', u'Lớp 4'),
                                              ('lop5', u'Lớp 5'), ('lop6', u'Lớp 6'), ('lop7', u'Lớp 7'), ('lop8', u'Lớp 8'),
                                              ('lop9', u'Lớp 9'), ('lop10', u'Lớp 10'), ('lop11', u'Lớp 11'), ('lop12', u'Lớp 12'), ('khac', u'Khác')])
    subject = TextField(u"Môn học:")
    message = TextAreaField(u"Yêu cầu:")
    submit = SubmitField(u"Gửi")
    
class RegisterTutorForm(Form):
    name = TextField(u"Họ Tên:", [validators.Required(u"Bạn chưa nhập tên")])
    email = TextField(u"Email:", [validators.Required(u"Bạn chưa nhập địa chỉ mail")])
    mobile = TextField(u"Số ĐT:", [validators.Required(u"Bạn chưa nhập số ĐT")])
    fbacc = TextField(u"Facebook:", [validators.Required(u"Bạn chưa nhập tài khoản Facebook")])
    college = TextField(u"Trường ĐH đang học:", [validators.Required(u"Bạn chưa nhập tên trường ĐH bạn đang học")])
    highschool = TextField(u"Trường THPT đã học:", [validators.Required(u"Bạn chưa nhập tên trường THPT bạn đã học")])
    subject = TextField(u"Môn học đăng kí dạy:", [validators.Required(u"Bạn chưa nhập môn học đăng kí dạy")])
    descriptions = TextAreaField(u"Thành tích, kinh nghiệm:", [validators.Required(u"Bạn chưa cung cấp thông tin thành tích, kinh nghiệm gia sư của bạn")])
    submit = SubmitField(u"Gửi")

    
    

# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, session, flash, redirect, url_for
import db, api
from forms import ContactForm, RegisterTutorForm



frontend = Blueprint('frontend', __name__,
                        template_folder='templates', static_folder='static')

PER_PAGE = 5

@frontend.route("/", methods=["GET", "POST"])
def home():
    return render_template("home.html")

@frontend.route("/contact", methods=["GET", "POST"])
def contact():
    form = ContactForm()
    
    if request.method == "POST":
        if form.validate() == False:
            flash(u'All fields are required.')
            return render_template('contact.html', form=form)
        else:
            # msg = Message(form.subject.data, sender='contact@example.com', recipients=['your_email@example.com'])
            msg = u"""
              *** Sent from Website giasuchuyenlamson.com ***
              Name: %s <%s>
              ĐT: %s
              Môn học: %s , Lớp: %s
              Yêu cầu: %s              
              """ % (form.name.data, form.email.data, form.mobile.data, form.subject.data, form.classroom.data, form.message.data)
            api.Send_mail(msg)
            return render_template("contact.html", success=True, form=form)
    elif request.method == "GET":
        return render_template("contact.html", form=form)

@frontend.route("/tutors", defaults={'page': 1})
@frontend.route("/tutors/<int:page>")
def tutor(page):
    count = db.count_all_tutors()
    tutors = db.get_tutors_for_page(page, PER_PAGE)
    pagination = api.Pagination(page, PER_PAGE, count)
    return render_template("tutors.html", tutors=tutors, pagination=pagination)


@frontend.route("/news", methods=["GET", "POST"])
def news():
    news = db.get_all_news()
    return render_template("news.html", news=news)

@frontend.route("/registertutor", methods=["GET", "POST"])
def registertutor():
    form = RegisterTutorForm()
    
    if request.method == "POST":
        if form.validate() == False:
            flash(u"Bạn chưa điền đầy đủ thông tin.")
            return render_template("registertutor.html", form=form)
        else:
            msg = u"""
              *** Sent From Website giasuchuyenlamson.com ***
              Name: %s <%s>
              ĐT: %s
              Facebook: %s
              Trường ĐH: %s
              THPT: %s
              Môn học: %s
              Thành tích, Kinh nghiệm: %s
            """ % (form.name.data, form.email.data, form.mobile.data, form.fbacc.data,
                  form.college.data, form.highschool.data, form.subject.data, form.descriptions.data)
            api.Send_mail(msg)
            return render_template("registertutor.html", success=True, form=form)
    elif request.method == "GET":
        return render_template("registertutor.html", form=form)

@frontend.route("/about", methods=["GET", "POST"])
def about():
    return render_template("about.html")

@frontend.route("/aboutlamson", methods=["GET", "POST"])
def aboutls():
    return render_template("aboutls.html")

@frontend.route("/document", methods=["GET", "POST"])
def document():
#     documents = db.get_new_documents()
#     return render_template("document.html", documents = documents)
    return render_template("learning.html")

@frontend.route("/displaydoc/<title>", methods=["GET", "POST"])
def displaydoc(title):
    document = db.get_document_bytitle(title)
    return render_template("displaydoc.html", document=document)

@frontend.route("/learning", methods=["GET", "POST"])
def learning():
    return render_template("learning.html")

@frontend.route("/price", methods=["GET", "POST"])
def price():
    return render_template("price.html")

@frontend.route("/classinfo", defaults={'page': 1})
@frontend.route("/classinfo/<int:page>")
def classinfo(page):
    count = db.count_all_class()
    classinfo = db.get_classinfo_for_page(page, 4)
    pagination = api.Pagination(page, 4, count)
    return render_template("classinfo.html", classinfo=classinfo, pagination=pagination)

@frontend.route("/registerclass", methods=["GET", "POST"])
def registerclass():
  if request.method == "POST":
    classid = request.form.get("classid")
    mobile = request.form.get("mobilerc")
    email = request.form.get("emailrc")
    tutor = db.get_tutor(mobile)
    if tutor:
      error = False
      message = u"""
      *** Sent from website ***
      %s (%s) đăng kí nhận lớp %s
      Email liên hệ: %s
      """ %(tutor.get("name"), tutor.get("tutorid"), classid, email)
      api.Send_mail(message)
      msg = "Bạn gửi đăng kí thành công. Chúng tôi sẽ xem xét đăng kí của bạn. Nếu bạn được nhận lớp, chúng tôi sẽ liên hệ với bạn theo số điện thoại và email mà bạn đã cung cấp."
    else:
      error = True
      msg = "Số điện thoại của bạn không hợp lệ hoặc do bạn chưa đăng kí làm gia sư của CLB."
      
  return render_template("notify.html", msg=msg, error=error)

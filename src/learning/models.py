# -*- coding: utf-8 -*-
import db


class Questions():
  def __init__(self, info):
    self.info = info
  
  @property
  def id(self):
    if self.info:
      return self.info.get("_id")
  
  @property
  def contents(self):
    if self.info:
      return self.info.get("contents")
  
  @property
  def tags(self):
    if self.info:
      return self.info.get("tags")
  
  @property
  def userid(self):
    if self.info:
      return self.info.get("userid")
    
  @property
  def answers(self):
    if self.info:
      return self.info.get("answers", [])
  
  @property
  def topic(self):
    if self.info:
      return self.info.get("topic")
  
  @property
  def like(self):
    if self.info:
      return self.info.get("like", [])
  
  @property
  def dislike(self):
    if self.info:
      return self.info.get("dislike", [])
  
  @property
  def followers(self):
    if self.info:
      return self.info.get("followers", [])
    
class Answers():
  def __init__(self, info):
    self.info = info
  
  @property
  def id(self):
    if self.info:
      return self.info.get("_id")
  
  @property
  def contents(self):
    if self.info:
      return self.info.get("contents")
  
  @property
  def userid(self):
    if self.info:
      return self.info.get("userid")
  
  @property
  def like(self):
    if self.info:
      return self.info.get("like", [])
    
  @property
  def dislike(self):
    if self.info:
      return self.info.get("dislike", [])
# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, flash, redirect, url_for
import db, api



learning = Blueprint('learning', __name__,
                        template_folder='templates', static_folder='static')

@learning.route("/newfeed", methods=["GET", "POST"])
@api.login_required
def newfeed():
  return render_template("learning/base.html")
# -*- coding: utf-8 -*-
import os
from functools import wraps
from flask import Flask, send_from_directory, url_for, request, redirect, session
from frontend import frontend
from admin import admin
from users import users
from learning import learning
import jinja2
import api, db

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

app = Flask(__name__)
# app.debug = True
app.register_blueprint(frontend)
app.register_blueprint(admin)
app.register_blueprint(users)
app.register_blueprint(learning)

app.secret_key = 'development key'

def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        session_id = session.get('session_id')
        if not session_id:
            return redirect('/login')
    
        user_id = db.get_user_id(session_id)
        if not user_id:
          return redirect('/login')
        return f(*args, **kwargs)
    return decorated_function

@app.route("/static/<path:filename>")
def public_files(filename):
    src = os.path.dirname(__file__)
    return send_from_directory(os.path.join(src, 'static'), filename)

if __name__ == "__main__":
  app.run()
#  app.run(host="127.0.0.1", port=5021, debug=True)

# -*- coding: utf-8 -*-
from pymongo import Connection
from pymongo import DESCENDING

Database = Connection("127.0.0.1:27017").lamsontutor
######### Add Tutor #######

data1 = {"_id":1301,
        "tutorid": "13001",
        "name":"Lê Văn Giang",
        "adults":"male",
        "college":"Đại học Ngoại Thương",
        "mobile": "01656057980",
        "email": "giangmanu1991@gmail.com",
        "subjects":["Toán", "Lý"],
        "urlimg":"img/tutor_img/gianglv.jpg",
        "quality":True,
        "descriptions":"Là học sinh chuyên Lý khóa 06-09, trường THPT chuyên Lam Sơn. Từng đạt giải nhất tỉnh môn vật lý năm lớp 9. Giải 3 môn vật lý trên tạp trí toán học tuổi trẻ năm 2009. Giải 3 cuộc thi tài chính chứng khoán I - Invest - Đại học ngoại thương tổ chức. Điểm thi đại học 2009 khối A : 27 điểm."}

data2 = {"_id":1302,
        "tutorid":"13002",
        "name":"Trần Thị Ngọc Ánh",
        "quality":False,
        "adults":"Nữ",
        "college":"Đại học Luật Hà Nội",
        "mobile": "0989019745",
        "email": "tranngocanh.141094@gmail.com",
        "subjects":["Văn", "Sử", "Địa"],
        "urlimg":"img/tutor_img/ngocanhtt.jpg",
        "descriptions":"Là học sinh chuyên Địa khóa 09-12, trường THPT chuyên Lam Sơn. Là người nhiệt tình, kiên nhẫn, nghiêm túc trong học tập, từng đạt giải nhì quốc gia môn Địa lý."}

data3 = {"_id":1303,
        "tutorid":"13003",
        "name":"Lê Vũ Thùy Trang",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "mobile": "0947582818",
        "email": "lvttrang2010@gmail.com",
        "subjects":["Tiếng Anh", "Toán"],
        "urlimg":"img/tutor_img/thuytranglv.jpg",
        "descriptions":"Là học sinh chuyên Anh khóa 08-11, trường THPT chuyên Lam Sơn. Từng đạt giải ba quốc gia và giải nhất thi HSG tỉnh Thanh Hóa môn Tiếng Anh."}

data4 = {"_id":1304,
        "name":"Đỗ Thùy Linh",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "mobile": "01668869686",
        "email": "thuylinhdo.5693@gmail.com",
        "subjects":["Tiếng Anh", "Toán"],
        "urlimg":"img/tutor_img/linhdt.jpg",
        "descriptions":"Là học sinh chuyên Anh khóa 08-11, trường THPT chuyên Lam Sơn. Từng đạt giải ba quốc gia và giải nhất thi HSG tỉnh Thanh Hóa môn Tiếng Anh. Điểm thi đại học khối D: 26."}

data5 = {"_id":1305,
        "name":"Võ Thị Trà My",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "subjects":["Văn"],
        "urlimg":"img/tutor_img/tramyvt.jpg",
        "descriptions":"Là học sinh chuyên Văn khóa 07-10, trường THPT chuyên Lam Sơn. Từng đạt giải nhì quốc gia môn Văn"}

data6 = {"_id":1306,
        "name":"Nguyễn Hạnh Hà My",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "subjects":["Văn"],
        "urlimg":"img/tutor_img/hamynh.jpg",
        "descriptions":"Là học sinh chuyên Văn khóa 08-11, trường THPT chuyên Lam Sơn. Từng đạt giải 3 quốc gia môn Văn."}

data7 = {"_id":1307,
        "name":"Trịnh Thị Hằng",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "subjects":["Văn"],
        "urlimg":"img/tutor_img/hangtt.jpg",
        "descriptions":"Là học sinh chuyên Văn khóa 07-10, trường THPT chuyên Lam Sơn. Từng đạt giải 3 quốc gia môn Văn."}


data8 = {"_id":1308,
        "name":"Trần Hà My",
        "adults":"Nữ",
        "birthday":"1992",
        "college":"Học viện Ngoại Giao",
        "subjects":["Hóa"],
        "urlimg":"img/tutor_img/myth.jpg",
        "descriptions":"Là học sinh chuyên Tin khóa 07-10, trường THPT chuyên Lam Sơn, Điểm thi Đh 26.5 (9.5 điểm Hóa), á khoa HV Ngoại Giao 2010. Đã có kinh nghiệm luyện thi ĐH môn hóa 2 năm."}

data9 = {"_id":1309,
        "name":"Đào Đức Huân",
        "adults":"Nam",
        "birthday":"1990",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán"],
        "urlimg":"img/tutor_img/huandd.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 09-12, trường THPT chuyên Lam Sơn. Đã có kinh nghiệm gia sư 3 năm."}


data10 = {"_id":1310,
        "name":"Đỗ Thị Diệp",
        "adults":"Nữ",
        "birthday":"1990",
        "college":"Đại học Y Hà Nội",
        "subjects":["Toán", "Sinh"],
        "urlimg":"img/tutor_img/diepdt.jpg",
        "descriptions":"Là học sinh chuyên Sinh khóa 05-08, trường THPT chuyên Lam Sơn. Giải nhì tỉnh môn sinh năm lớp 9, giải KK tỉnh môn sinh năm lớp 12, thi ĐH môn sinh 9,5."}

data11 = {"_id":1311,
        "name":"Lê Thị Thu Hiền",
        "adults":"Nữ",
        "birthday":"1994",
        "college":"Đại học Ngoại Thương",
        "subjects":["Tiếng Anh", "Toán"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Anh khóa 09-12, trường THPT chuyên Lam Sơn. Giải 3 học sinh giỏi Tiếng Anh cấp tỉnh lớp 9. có kinh nghiệm gia sư lâu năm cho các em lớp  3-7, có thể dạy được những em nghịch ngợm, lười học, nhiệt tình tâm huyết."}

data12 = {"_id":1312,
        "name":"Trịnh Thị Hằng",
        "adults":"Nữ",
        "birthday":"1992",
        "college":"Đại học Ngoại Thương",
        "subjects":["Văn"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Văn khóa 07-10, trường THPT chuyên Lam Sơn. Giải ba học sinh giỏi quốc gia môn Văn, giải nhất học sinh giỏi tỉnh môn Văn."}

data13 = {"_id":1313,
        "name":"Hồ Thị Ngọc",
        "adults":"Nữ",
        "birthday":"1992",
        "college":"ĐH Khoa học XH & NV",
        "subjects":["Toán", "Văn", "Tiếng Anh"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Văn khóa 07-10, trường THPT chuyên Lam Sơn. Giải nhì tỉnh môn văn, giải 3 viết văn toàn trường chào mừng ngày 20/11."}

data14 = {"_id":1314,
        "name":"La Hồng Quân",
        "adults":"Nam",
        "birthday":"1993",
        "college":"Đại học Ngoại Thương",
        "subjects":["Toán"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Toán khóa 08-11, trường THPT chuyên Lam Sơn. Giải 3 toán quốc gia năm 2011, Giải nhất báo toán học tuổi trẻ năm 2007-2008; Giải nhì các năm 2008-2009 và 2009-2010"}

data15 = {"_id":1315,
        "name":"Lê Tuấn Vũ",
        "adults":"Nam",
        "birthday":"1991",
        "college":"Đại học BKHN",
        "subjects":["Toán", "Tin"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Tin khóa 06-09, trường THPT chuyên Lam Sơn. Giải ba cấp tỉnh môn Vật Lý."}

data16 = {"_id":1316,
        "name":"Trần Thị Nhâm",
        "adults":"Nữ",
        "birthday":"1993",
        "college":"Đại học Ngoại Thương",
        "subjects":["Toán", "Văn", "Tiếng Anh"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Anh khóa 08-11, trường THPT chuyên Lam Sơn. Giải nhất cấp tỉnh lớp 9 và lớp 12 môn Tiếng Anh. Giải Ba cấp huyện môn Văn."}

data17 = {"_id":1317,
        "name":"Trần Thị Ngọc Ánh",
        "adults":"Nữ",
        "birthday":"1994",
        "college":"Đại học Luật HN",
        "subjects":["Văn", "Sử", "Địa"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Địa khóa 09-12, trường THPT chuyên Lam Sơn. Giải Nhì học sinh giỏi Quốc gia môn Địa lí lớp 12, giải Nhất học sinh giỏi cấp tỉnh môn Địa lí lớp 12, giải khuyến khích học sinh giỏi cấp tỉnh môn Văn lớp 9"}

data18 = {"_id":1318,
        "name":"Bùi Anh Tú",
        "adults":"Nam",
        "birthday":"1994",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán", "Lý", "Tiếng Anh"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Lý khóa 09-12, trường THPT chuyên Lam Sơn. Giải khuyến khích quốc gia lý, 6.5 Ielts. Điểm thi đại học 24"}

data19 = {"_id":1319,
        "name":"Nguyễn Thùy Duyên",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "subjects":["Tiếng Anh"],
        "urlimg":"img/tutor_img/duyennt.jpg",
        "descriptions":"Là học sinh chuyên Anh khóa 09-12, trường THPT chuyên Lam Sơn."}

data20 = {"_id":1320,
        "name":"Đỗ Ngọc Dương",
        "adults":"Nam",
        "birthday":"1990",
        "college":"Đại học Ngoại Thương",
        "subjects":[],
        "urlimg":"img/tutor_img/duongdn.jpg",
        "descriptions":"Là học sinh chuyên Nga khóa 05-08, trường THPT chuyên Lam Sơn."}

data21 = {"_id":1321,
        "name":"Ngô Thu Hà",
        "adults":"Nữ",
        "college":"Học viện Tài Chính",
        "subjects":["Toán", "Tiếng Anh"],
        "urlimg":"img/tutor_img/hant.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 06-09, trường THPT chuyên Lam Sơn."}

data22 = {"_id":1322,
        "name":"Lê Mạnh Hùng",
        "adults":"Nam",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán"],
        "urlimg":"img/tutor_img/hunglm.jpg",
        "descriptions":"Là học sinh chuyên Sinh khóa 07-10, trường THPT chuyên Lam Sơn."}

data23 = {"_id":1323,
        "name":"Đỗ Xuân Khánh",
        "adults":"Nam",
        "college":"Đã tốt nghiệp",
        "subjects":["Toán"],
        "urlimg":"img/tutor_img/khanhdx.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 05-08, trường THPT chuyên Lam Sơn."}

data24 = {"_id":1324,
        "name":"Lê Thị Thùy Linh",
        "adults":"Nữ",
        "college":"Đại học Ngoại Thương",
        "subjects":["Toán"],
        "urlimg":"img/tutor_img/linhltt.jpg",
        "descriptions":"Là học sinh chuyên Nga khóa 07-10, trường THPT chuyên Lam Sơn."}

data25 = {"_id":1325,
        "name":"Tào Lê Minh",
        "adults":"Nam",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán", "Tiếng Anh"],
        "urlimg":"img/tutor_img/minhlt.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 09-12, trường THPT chuyên Lam Sơn."}

data26 = {"_id":1326,
        "name":"Phạm Thị Phương",
        "adults":"Nữ",
        "college":"Khoa sư phạm Anh - Đại học Ngoại Ngữ",
        "subjects":["Tiếng Anh"],
        "urlimg":"img/tutor_img/phuongpt.jpg",
        "descriptions":"Là học sinh chuyên Văn khóa 08-11, trường THPT chuyên Lam Sơn."}

data27 = {"_id":1327,
        "name":"Đỗ Mạnh Quang",
        "adults":"Nam",
        "college":"Đại học Y Hà Nội",
        "mobile":"0982414379",
        "email":"domanhquanghmu@gmail.com",
        "subjects":["Toán", "Hóa"],
        "urlimg":"img/tutor_img/quangdm.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 05-08, trường THPT chuyên Lam Sơn."}

data28 = {"_id":1328,
        "name":"Vũ Xuân Tâm",
        "adults":"Nữ",
        "college":"Học Viện Ngân Hàng",
        "subjects":["Tiếng Anh"],
        "urlimg":"img/tutor_img/tamvx.jpg",
        "descriptions":"Là học sinh chuyên Anh khóa 06-09, trường THPT chuyên Lam Sơn."}

data29 = {"_id":1329,
        "name":"Nguyễn Trần Tiến",
        "adults":"Nam",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán", "Lý"],
        "urlimg":"img/tutor_img/tiennt.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 09-12, trường THPT chuyên Lam Sơn."}

data30 = {"_id":1330,
        "name":"Cao Thị Thanh",
        "adults":"Nữ",
        "birthday":"1993",
        "college":"Đại học Ngoại Ngữ, ĐHQG HN",
        "subjects":["Toán", "Anh"],
        "urlimg":"img/tutor_img/thanhct.jpg",
        "descriptions":"Là học sinh chuyên Anh khóa 08-11, trường THPT chuyên Lam Sơn."}

data31 = {"_id":1331,
        "name":"Lê Thiện Giáp",
        "adults":"Nam",
        "birthday":"1994",
        "college":"Đại học Dược HN",
        "subjects":["Toán", "Hóa"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Toán khóa 09-12, trường THPT chuyên Lam Sơn."}

data32 = {"_id":1332,
        "name":"Nguyễn Thanh Bình",
        "adults":"Nam",
        "birthday":"1994",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán"],
        "urlimg":"img/tutor_img/binhnt.jpg",
        "descriptions":"Là học sinh chuyên Toán khóa 09-12, trường THPT chuyên Lam Sơn."}

data33 = {"_id":1333,
        "name":"Đào Thị Mai Phương",
        "adults":"Nữ",
        "birthday":"1994",
        "college":"Đại học Kinh Tế Quốc Dân",
        "subjects":["Toán", "Văn"],
        "urlimg":"img/tutor_img/phuongdtm.jpg",
        "descriptions":"Là học sinh chuyên Nga khóa 09-12, trường THPT chuyên Lam Sơn."}

data34 = {"_id":1334,
        "name":"Nguyễn Thị Thảo",
        "adults":"Nữ",
        "birthday":"1993",
        "college":"Đại học Ngoại Thương",
        "subjects":["Văn"],
        "urlimg":"img/tutor_img/no-avatar.png",
        "descriptions":"Là học sinh chuyên Văn khóa 08-11, trường THPT chuyên Lam Sơn."}

Database.tutor.save(data1)
Database.tutor.save(data2)
Database.tutor.save(data3)
Database.tutor.save(data4)
Database.tutor.save(data5)
Database.tutor.save(data6)
Database.tutor.save(data7)
Database.tutor.save(data8)
Database.tutor.save(data9)
Database.tutor.save(data10)
Database.tutor.save(data11)
Database.tutor.save(data12)
Database.tutor.save(data13)
Database.tutor.save(data14)
Database.tutor.save(data15)
Database.tutor.save(data16)
Database.tutor.save(data17)
Database.tutor.save(data18)
Database.tutor.save(data19)
Database.tutor.save(data20)
Database.tutor.save(data21)
Database.tutor.save(data22)
Database.tutor.save(data23)
Database.tutor.save(data24)
Database.tutor.save(data25)
Database.tutor.save(data26)
Database.tutor.save(data27)
Database.tutor.save(data28)
Database.tutor.save(data29)
Database.tutor.save(data30)
Database.tutor.save(data31)
Database.tutor.save(data32)
Database.tutor.save(data33)
Database.tutor.save(data34)

######### Add document ################
# data = {"_id": 130001,
#        "title": "Tài liệu Flask microframework",
#        "date":"2/5/2013",
#        "subjects": "IT",
#        "categories":"Tài liệu",
#        "linkscribd":"http://www.scribd.com/embeds/138971540/content?start_page=1&view_mode=slideshow&access_key=key-i74gyu12c6yrrndlozv",
#        "linkdownload":"mediafire",
#        "tags":["python", "flask", "tài liệu", "microframework", "lập trình"]}
# Database.document.save(data)

######### Update Info tutor ###########
# Database.tutor.update({"_id":1309}, {"$set":{"subjects":["Toán"]}})
# Database.tutor.update({"_id":1310}, {"$set":{"subjects":["Toán"]}})
# Database.tutor.update({"_id":1311}, {"$set":{"subjects":["Toán"]}})
# Database.tutor.update({"_id":1313}, {"$set":{"subjects":["Tiếng Anh"]}})
# Database.tutor.update({"_id":1315}, {"$set":{"subjects":["Tiếng Anh"]}})
# Database.tutor.update({"_id":1304}, {"$set":{"urlimg":"img/tutor_img/tramyvt.jpg"}})
# Database.tutor.update({"_id":1305}, {"$set":{"urlimg":"img/tutor_img/thult.jpg"}})
# Database.tutor.update({"_id":1306}, {"$set":{"urlimg":"img/tutor_img/hamynh.jpg"}})

######### Add News #########

# data = {"_id":"201301",
#        "date":"21/04/2013",
#        "title":'CLB Gia sư chuyên Lam Sơn ra mắt chuyên mục "Góc học tập"',
#        "content": """Nhằm nâng cao chất lượng học tập cho các em học sinh của CLB. Gia sư chuyên Lam Sơn ra mắt chuyên mục "Góc học tập" tại địa chỉ : http://on.fb.me/15vUcms. Chuyên mục "Góc học tập" là nơi trao đổi các phương pháp học tập, những tài liệu bổ ích, là nơi hỏi đáp về những vấn đề mà các em học sinh còn thắc mắc trong quá trình học tập. Tuy nhiên, do hiện nay facebook không hỗ trợ các công cụ viết công thức Toán học, Vật lý, Hóa học... như LaTex hay MathML, vì vậy chúng tôi sẽ triển khai xây dựng "Góc học tập" ngay trên trang web của CLB nhằm hạn chế những nhược điểm trên."""}
# Database.news.save(data)

# Database.tutor.update({"_id":201301}, {"$set":{"content":"""Nhằm nâng cao chất lượng học tập cho các em học sinh của CLB. Gia sư chuyên Lam Sơn ra mắt chuyên mục "Góc học tập" tại địa chỉ : <a href='http://on.fb.me/15vUcms'>http://on.fb.me/15vUcms</a>. Chuyên mục "Góc học tập" là nơi trao đổi các phương pháp học tập, những tài liệu bổ ích, là nơi hỏi đáp về những vấn đề mà các em học sinh còn thắc mắc trong quá trình học tập. Tuy nhiên, do hiện nay facebook không hỗ trợ các công cụ viết công thức Toán học, Vật lý, Hóa học... như LaTex hay MathML, vì vậy chúng tôi sẽ triển khai xây dựng "Góc học tập" ngay trên trang web của CLB nhằm hạn chế những nhược điểm trên."""}})

######## Update News ########



######## Test ##########


# -*- coding: utf-8 -*-
from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField, SubmitField, SelectField, validators, ValidationError, PasswordField, BooleanField

class LoginForm(Form):
    username = TextField(u"Username:")
    password = PasswordField(u"Password:")
    rememberme = BooleanField(u"Remember Me")
    submit = SubmitField(u"Login")

class RegisterForm(Form):
    name = TextField(u"Your name:")
    email = TextField(u"Email:")
    password = PasswordField(u"Password:")
    submit = SubmitField(u"Signup") 

# -*- coding: utf-8 -*-
import db

class User:
  def __init__(self, info):
    self.info = info
    
  @property
  def id(self):
    if self.info:
      return self.info.get("_id")

  @property
  def name(self):
    if self.info:
      return self.info.get("name")
  
  @property
  def email(self):
    if self.info:
      return self.info.get("email")
  
  @property
  def password(self):
    if self.info:
      return self.info.get("password")
  
  @property
  def avatar(self):
    if self.info:
      return self.info.get("avatar")
  
  @property
  def gender(self):
    if self.info:
      return self.info.get("gender")
  
  @property
  def birthday(self):
    if self.info:
      return self.info.get("birthday")
  
  @property
  def join_at(self):
    if self.info:
      return self.info.get("join_at")
  
  @property
  def followers(self):
    if self.info:
      return self.info.get("followers", [])
  
  @property
  def following(self):
    if self.info:
      return self.info.get("following", [])
    
  @property
  def topic(self):
    if self.info:
      return self.info.get("topic", [])
  
  @property
  def last_login(self):
    if self.info:
      return self.info.get("last_login")
  
  @property
  def message(self):
    if self.info:
      return self.info.get("message", [])
  
  @property
  def notify(self):
    if self.info:
      return self.info.get("notify", [])
  
  @property
  def session_id(self):
    if self.info:
      return self.info.get("session_id", [])
  

    
    

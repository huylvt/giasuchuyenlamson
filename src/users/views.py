# -*- coding : utf-8 -*-
from flask import (Blueprint, render_template, request, redirect,
                   make_response, flash, session)
from forms import LoginForm
from uuid import uuid4
import hashlib
import db


users = Blueprint("users", __name__, template_folder='templates',
                  static_folder="static")

@users.route("/login", methods=["GET", "POST"])
def login():
    message = ""
    if request.method == "POST":
        username = request.form.get('username',
                                  request.args.get('username'))
        password = request.form.get('password')
        
        if not username or not password:
            message = "Please fill all the fields."
            return render_template("login.html", message=message)
        if username == "admin":
          session_id = db.sign_in(username, password)
        else:
          session_id = db.login(username, password)
        if not session_id:
            message = "Username or Password is invaild."
            return render_template("login.html", message=message)
        
        session['session_id'] = session_id
        if username == "admin":
          return redirect("/dashboard")
        else: 
          return redirect("/learningpage")
    else:
        return render_template("login.html", message=message)

@users.route("/logout", methods=["GET", "POST"])
def logout():
  session_id = session.get('session_id')
  if session_id:
    db.logout(session_id)
    session.pop('session_id')
  return redirect('/')
